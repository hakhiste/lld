# Project Title
LLD - Low Level Design

## Description

This is for learning how to implement the LLD in PHP.

## Installation

Install WAMP/LAMP & run ObserverPattern/index.php

## Usage

1. Observer Design Pattern - ObaserverPattern

2. Decorator Design Pattern - DecoratorDesignPattern