<?php
/**
 * this file contains observer design pattern example code base
 * this pattern is combination of subscriber & notifier pattern
 * 
 * Example:
 * As soon as product is in stock then user should receive notification
 */

include 'autoloader.php';

$objMobileObservable = new MobileObserverable();

$objObserver = new NotifyObserver('hakhiste@gmail.com', $objMobileObservable);
$objObserver2 = new NotifyObserver('xyz@gmail.com', $objMobileObservable);
$objObserver3 = new NotifyObserver('abc@gmail.com', $objMobileObservable);

// subscribe
$objMobileObservable->add($objObserver);
$objMobileObservable->add($objObserver2);
$objMobileObservable->add($objObserver3);

// update observers
$objMobileObservable->updateStockCount(20);
