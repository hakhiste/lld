<?php 
interface ObserverInterface {
    public function update();
}

class NotifyObserver implements ObserverInterface {
    public $email;
    public $objObservable;

    public function __construct($email, $objObservable) {
        $this->email = $email;
        $this->objObservable = $objObservable;
    }

    /**
     * This method is called when the observed object is updated.
     * It sends an email notification to the user indicating that the product is in stock.
     *
     * @return void
     */
    public function update() {
        echo '<br>'.$this->email." is in stock now.";
    }
}