<?php
interface StockObserverInterface {
    public function add($objObserver);
    public function notify();
    public function updateStockCount($stock);
}

class MobileObserverable implements StockObserverInterface {
    private $observerList;
    private $stock;
    public function __construct() {
        $this->stock = 0; 
    }

    /**
     * Adds an observer to the list of observers.
     *
     * This method allows an observer to register itself with the MobileObserverable object.
     * When the MobileObserverable object's state changes, it will notify all registered observers.
     *
     * @param StockObserverInterface $objObserver The observer to be added.
     * @return void
     */
    public function add($objObserver) {
        $this->observerList[] = $objObserver;
    }

    /**
     * Notify all registered observers about the stock status change.
     *
     * This method iterates over all registered observers and calls their update method.
     * If there are no observers registered, it does nothing.
     *
     * @return void
     */
    public function notify() {
        if(!empty($this->observerList)) {
            foreach($this->observerList as $objObserver) {
                $objObserver->update();
            }
        }
    }


    /**
     * Updates the stock count and notifies observers if the stock goes from 0 to a positive value.
     *
     * This method checks if the current stock count is 0 and the new stock count is greater than 0.
     * If this condition is met, it calls the notify method to inform all registered observers about the stock status change.
     * Finally, it updates the stock count with the new value.
     *
     * @param int $stock The new stock count.
     * @return void
     */
    public function updateStockCount($stock) {
        if($this->stock == 0 & $stock > 0) {
            $this->notify();
        }
        $this->stock = $stock;
    }
}