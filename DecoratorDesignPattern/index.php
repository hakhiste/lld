<?php
/**
 * this file contains Decorator design pattern example code base
 * this pattern is combination of subscriber & notifier pattern
 * 
 * Example:
 * Pizza order
 */

include 'autoloader.php';


$topping = new VegetableTopping(new CheeseTopping(new CheeseTopping(new Farmhouse)));
echo 'Total cost for Farmhouse + double cheese + vegetable toppings = '. $topping->cost();

$topping2 = new VegetableTopping(new CheeseTopping(new Margarita));
echo '<hr />Total cost for Margarita + cheese + vegetable toppings = '. $topping2->cost();