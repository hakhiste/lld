<?php
include_once('Toping.php');


class CheeseTopping extends Toping {
    public $basePizza;

    public function __construct(BasePizza $pizza) {
        $this->basePizza = $pizza;
    }

    public function cost() {
        return $this->basePizza->cost() + 30;
    }
}